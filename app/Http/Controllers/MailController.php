<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\ContactFormRequest;

class MailController extends Controller
{

  public function create()
  {
      return view('contact.create');
  }

  public function store(ContactFormRequest $request)
  {
    $contact = [];
    $contact['name'] = $request->get('name');
    $contact['email'] = $request->get('email');
    $contact['msg'] = $request->get('msg');

    Mail::to('bychrisr@gmail.com')->send(new SendMail($contact));

    return 'Email was sent';
  }
}
