<!-- ARQUIVO QUE CONFIGURA O LAYOUT TODO -->

<!doctype html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>@yield('title')</title>
  <meta name="description" content="">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
  <!-- Themify Icons -->
  <link rel="stylesheet" href="{{asset('css/themify-icons.css')}}">
  <!-- Owl carousel -->
  <link rel="stylesheet" href="{{asset('css/owl.carousel.min.css')}}">
  <!-- Main css -->
  <link rel="stylesheet" href="{{asset('css/style.css')}}">

  <!-- Font -->
  <link rel="dns-prefetch" href="//fonts.googleapis.com">
  <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">

  <link rel="apple-touch-icon" sizes="57x57" href="{{asset('img/favicon/apple-icon-57x57.png')}}">
  <link rel="apple-touch-icon" sizes="60x60" href="{{asset('img/favicon/apple-icon-60x60.png')}}">
  <link rel="apple-touch-icon" sizes="72x72" href="{{asset('img/favicon/apple-icon-72x72.png')}}">
  <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/favicon/apple-icon-76x76.png')}}">
  <link rel="apple-touch-icon" sizes="114x114" href="{{asset('img/favicon/apple-icon-114x114.png')}}">
  <link rel="apple-touch-icon" sizes="120x120" href="{{asset('img/favicon/apple-icon-120x120.png')}}">
  <link rel="apple-touch-icon" sizes="144x144" href="{{asset('img/favicon/apple-icon-144x144.png')}}">
  <link rel="apple-touch-icon" sizes="152x152" href="{{asset('img/favicon/apple-icon-152x152.png')}}">
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicon/apple-icon-180x180.png')}}">
  <link rel="icon" type="image/png" sizes="192x192"  href="{{asset('img/favicon/android-icon-192x192.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicon/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="96x96" href="{{asset('img/favicon/favicon-96x96.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicon/favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset('img/favicon/manifest.json')}}">
  <meta name="msapplication-TileColor" content="#5e2129">
  <meta name="msapplication-TileImage" content="{{asset('img/favicon/ms-icon-144x144.png')}}">
  <meta name="theme-color" content="#5e2129">

  <!-- Social -->
  <meta property="og:locale" content="pt_BR">
  <meta property="og:title" content="Page Title" />
  <meta property="og:site_name" content="Lorem Ipsum">
  <meta property="og:description" content="Page content description" />
  <meta property="og:type" content="website">
  <meta property="og:url" content="http://construtorafazendatexas.com.br/" />
  <meta property="og:image" content="{{asset('img/imgOG.jpg')}}">
  <meta property="og:image:type" content="image/jpeg">
  <meta property="og:image:width" content="800">
  <meta property="og:image:height" content="600">

  <meta name="robots" content="index, follow">

</head>

<body data-spy="scroll" data-target="#navbar" data-offset="30">
  @section('header')
    <div class="nav-menu fixed-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-dark navbar-expand-lg">
                        <a class="navbar-brand" href="#home">
                          <img src="images/logo.png" class="img-fluid d-none d-lg-block" alt="logo">
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                          <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbar">
                            <ul class="navbar-nav ml-auto">
                                <li class="nav-item d-lg-none"><img src="{{asset('img/logo.png')}}" /></li>
                                <li class="nav-item"> <a class="nav-link active" href="#home">HOME <span class="sr-only">(current)</span></a> </li>
                                <li class="nav-item"> <a class="nav-link" href="#sobre">SOBRE</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="#gallery">PROJETOS</a> </li>
                                <li class="nav-item"> <a class="nav-link" href="#contact">ORÇAMENTO</a> </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
  @show <!-- HEADER -->

  <!-- CHAMA O CONTEÚDO -->
  @yield('content')

  <!-- FOOTER -->
  @section('footer')
  <footer class="text-center">
    <p class="mb-2">
      <small>COPYRIGHT © 2018. ALL RIGHTS RESERVED.<br>
        DESENVOLVIDO POR <a href="https://christianrodrigues.yes.es">CHRIS R.</a>
      </small>
    </p>
  </footer>
  @show <!-- FOOTER -->

  <!-- jQuery e Bootstrap -->
  <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
  <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
  <!-- JS Customizado -->
  <script src="{{asset('js/script.js')}}"></script>

</body>
</html>
