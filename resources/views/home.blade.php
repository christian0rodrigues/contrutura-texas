@extends('layouts.layout') <!-- Chamando o arquivo layout dentro da pasta layouts -->

@section('title', 'Fazenda Texas') <!-- Título da Section -->

<!-- Carrega o header -->
@section('header')
@parent
@endsection

<!-- Carrega o conteúdo -->
@section('content')

<img src="{{asset('img/cover.jpg')}}" class="img-fluid" alt="Responsive image" id="home">
<!-- <header class="bg-gradient" id="home">
  <div class="container mt-5">
    <h1>Fazenda Texas</h1>
    <h2>Construtora & Incorporadora</h2>
  </div>
  <div class="row justify-content-center solicitarOrcamento">
    <div class="col-5">
      <img src="images/cover.png" alt="phone" class="img-fluid">
      <a href="#contact" class="btn btn-primary btn-lg btn-block">SOLICITAR ORÇAMENTO</a>
    </div>
  </div>
</header> -->

<div class="section " id="sobre">
  <div class="container">
    <div class="section-title">
      <h3>SOBRE</h3>
    </div>

    <div class="tab-content">
      <div class="jumbotron">
        <p class="lead">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis lobortis dolor, id dictum urna. Sed lacinia at lectus ut posuere. Nam lobortis turpis nulla. Sed pretium lacinia ligula vel convallis. Vivamus non vulputate mi. Donec ultricies risus nec purus tincidunt, eu imperdiet nibh ultricies. Ut luctus tellus eget viverra congue. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mi magna, placerat ac ullamcorper nec, auctor molestie leo. Suspendisse efficitur, nibh ut fringilla scelerisque, lacus enim malesuada urna, nec vehicula orci ipsum at odio. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam ex libero, malesuada sodales tempor quis, mollis eu elit. Nunc convallis ex at convallis elementum. Quisque vel leo placerat, pellentesque elit id, blandit metus. Nullam ac dictum magna. Mauris imperdiet fermentum orci.
          <br>
          Fusce placerat ornare libero ut consequat. Proin rhoncus ex sit amet lectus feugiat, feugiat ullamcorper leo dignissim. Proin nulla metus, maximus a mauris at, mattis ullamcorper risus. Aenean imperdiet dapibus neque nec vulputate. In maximus orci nibh, sed fermentum ipsum imperdiet et. Vestibulum vitae elementum lectus. Mauris nec sollicitudin dui. Quisque quis dui vitae leo euismod vehicula. Proin ut sem id orci vulputate gravida et ut mi. Quisque semper ullamcorper nunc, non molestie nisl pellentesque in. Vestibulum fermentum dui sit amet mi porttitor, at iaculis turpis porttitor. Vivamus suscipit nisi arcu, facilisis vestibulum est vehicula condimentum.
          <br>
          Sed facilisis, libero quis dictum ultrices, odio risus hendrerit neque, nec feugiat neque libero at arcu. Etiam rutrum molestie ultricies. In eget fermentum libero. Donec nunc velit, volutpat ac elit porttitor, gravida aliquet urna. Etiam vehicula aliquam nulla at eleifend. Nulla scelerisque massa eu quam bibendum pellentesque. Nulla at viverra dolor. Curabitur sit amet ipsum mi. Fusce quis tempor tellus.
          <br>
          Maecenas sed finibus diam, eu mattis ligula. Curabitur sagittis ipsum ac arcu volutpat posuere ut in ante. Nulla eu vestibulum lacus, non ullamcorper est. Ut faucibus felis quis quam ultrices, id cursus risus hendrerit. Suspendisse interdum erat at ligula tristique, at lobortis arcu cursus. Vestibulum dignissim lacus in lectus malesuada, at bibendum magna tempor. Vivamus facilisis ex at volutpat feugiat. Praesent lacus risus, dictum at dui id, varius ullamcorper nisi. Duis et sem eget nulla consectetur lacinia. Ut scelerisque in odio et accumsan. Fusce a ligula finibus, rhoncus mauris tempor, pretium magna. Morbi purus lectus, mollis in commodo eget, fringilla id nisl. Etiam a pharetra arcu, at laoreet leo. Donec purus purus, mollis eu justo vitae, tristique fringilla purus. Nunc a nibh quis felis laoreet consectetur ut in eros. Duis consequat elit ac turpis fringilla, ut finibus tellus ultricies.
        </p>
      </div> <!-- jumbotron -->
    </div> <!-- tab-content -->
  </div> <!-- container -->
</div>
<!-- // end .section -->


<div class="section" id="gallery">
  <div class="container">
    <div class="section-title">
      <h3>PROJETOS</h3>
    </div>
  </div>

<!--
####################################################
C A R O U S E L
####################################################
-->
<div id="carousel" class="carousel slide carousel-showmanymoveone" data-ride="carousel" data-interval="6000">

  <div class="carousel-inner row no-gutters w-100 mx-auto" role="listbox">

    <div class="carousel-item active col-sm-4 col-md-3">
      <a href="https://bootstrapcreative.com/">
        <img src="{{asset('img/projetos/projeto01.jpg')}}" alt="responsive image" class="d-block img-fluid">
      </a>
    </div>

    <div class="carousel-item col-sm-4 col-md-3">
      <a href="https://bootstrapcreative.com/">
        <img src="{{asset('img/projetos/projeto02.jpg')}}" alt="responsive image" class="d-block img-fluid">
      </a>
    </div>

    <div class="carousel-item col-sm-4 col-md-3">
      <a href="https://bootstrapcreative.com/">
        <img src="{{asset('img/projetos/projeto03.jpg')}}" alt="responsive image" class="d-block img-fluid">
      </a>
    </div>

    <div class="carousel-item col-sm-4 col-md-3">
      <a href="https://bootstrapcreative.com/">
        <img src="{{asset('img/projetos/projeto04.jpg')}}" alt="responsive image" class="d-block img-fluid">
      </a>
    </div>

    <div class="carousel-item col-sm-4 col-md-3">
      <a href="https://bootstrapcreative.com/">
        <img src="{{asset('img/projetos/projeto05.jpg')}}" alt="responsive image" class="d-block img-fluid">
      </a>
    </div>

    <div class="carousel-item col-sm-4 col-md-3">
      <a href="https://bootstrapcreative.com/">
        <img src="{{asset('img/projetos/projeto06.jpg')}}" alt="responsive image" class="d-block img-fluid">
      </a>
    </div>

  </div>
  <!-- /.carousel-inner -->
        <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <!-- /.carousel -->
</div><!-- /.carousel -->

</div> <!-- // end .section -->

<div class="section py-5 light-bg" id="contact">

  <div class="container">

    <div class="section-title">
      <h3>SOLICITE SEU ORÇAMENTO</h3>
    </div>

    <!-- <form>
      <div class="form-group">
        <label for="name">Nome</label>
        <input type="text" class="form-control" id="name" autocomplete="off">
      </div>
      <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" autocomplete="off">
      </div>
      <div class="form-group">
        <label for="exampleFormControlTextarea1">Descreva o projeto</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="12"></textarea>
      </div>
      <button type="submit" class="btn btn-primary btn-lg btn-block">Enviar</button>
    </form> -->

    {!! Form::open(['route' => 'home.store']) !!}

    <div class="form-group">
        {!! Form::label('name', 'Nome') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('msg', 'Mensagem') !!}
        {!! Form::textarea('msg', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
    </div>

    {!! Form::submit('Enviar', ['class' => 'btn btn-primary btn-lg btn-block']) !!}

    {!! Form::close() !!}

  </div> <!-- end .container -->

</div>
<!-- // end .section -->

@endsection
